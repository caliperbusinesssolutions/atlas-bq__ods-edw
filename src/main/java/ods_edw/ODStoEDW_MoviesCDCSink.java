package ods_edw;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.api.services.bigquery.model.TableReference;
import com.google.api.services.bigquery.model.TableRow;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;


import org.apache.beam.sdk.Pipeline;
import org.apache.beam.sdk.extensions.gcp.options.GcpOptions;
import org.apache.beam.sdk.io.gcp.bigquery.BigQueryIO;
import org.apache.beam.sdk.io.gcp.bigquery.BigQueryIO.Write.Method;
import org.apache.beam.sdk.io.gcp.pubsub.PubsubIO;
import org.apache.beam.sdk.options.*;
import org.apache.beam.sdk.options.Validation.Required;
import org.apache.beam.sdk.transforms.MapElements;
import org.apache.beam.sdk.transforms.SimpleFunction;
import org.bson.Document;

import java.io.IOException;
import java.math.BigInteger;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import com.google.api.services.bigquery.model.TableSchema;
import com.google.api.services.bigquery.model.TableFieldSchema;

public class ODStoEDW_MoviesCDCSink {

    public static void main(String[] args) throws IOException {
        // The maximum number of shards when writing output.
        int numShards = 1;
       /* TableSchema schema =
        		TableSchema.of(
        				TableFieldSchema.of("stringField", StandardSQLTypeName.STRING),
        				TableFieldSchema.of("booleanField", StandardSQLTypeName.BOOL));*/
        
        /*  List<TableFieldSchema> fields = new ArrayList<>();
        fields.add(new TableFieldSchema().setName("month").setType("INTEGER"));
        fields.add(new TableFieldSchema().setName("tornado_count").setType("INTEGER"));
        TableSchema schema = new TableSchema().setFields(fields);*/
       // TableSchema schema = new TableSchema();


        PubSubToGCSOptions options = PipelineOptionsFactory
                .fromArgs(args)
                .withValidation()
                .as(PubSubToGCSOptions.class);

        options.setStreaming(true);

        TableReference table1 = new TableReference();
        table1.setProjectId(options.getProject());
        table1.setDatasetId("dStream");
        table1.setTableId("indent_vehicle_collection");

        Pipeline pipeline = Pipeline.create(options);
       // System.out.print("pipeline...."+pipeline);
        pipeline
                .apply("Read PubSub Messages", PubsubIO.readStrings().fromTopic(options.getInputTopic()))
                .apply("Read and transform Movies data", MapElements.via(
                        new SimpleFunction<String, TableRow>() {
                            @Override
                            public TableRow apply(String document) {
                                Gson gson = new GsonBuilder().create();
                                //System.out.println("document...."+document);
                                HashMap<String, Object> parsedMap = gson.fromJson(document, HashMap.class);
                                //schema.putAll(parsedMap);
                                TableRow row = new TableRow()
                               // row.putAll(parsedMap);
                                		 .set("company_id", parsedMap.containsKey("company_id")?parsedMap.get("company_id").toString():null)
                                                .set("indent_id", parsedMap.containsKey("indent_id")?parsedMap.get("indent_id").toString():null)
                                                .set("token", parsedMap.containsKey("token")?parsedMap.get("token").toString():null)
                                                .set("trip_number", parsedMap.containsKey("trip_number")? (int)parsedMap.get("trip_number"):null)
                                                .set("planning_point", parsedMap.containsKey("planning_point")?parsedMap.get("planning_point").toString():null)
                                                .set("is_active",   parsedMap.containsKey("is_active")?Boolean.parseBoolean(parsedMap.get("is_active").toString()):null)
                                                .set("contract_tenure_from",   parsedMap.containsKey("contract_tenure_from")?parsedMap.get("contract_tenure_from"):null)
                                               // .set("inserted_on_bq", LocalDateTime.now());
                                  .set("contract_tenure_to",   parsedMap.containsKey("contract_tenure_to")?parsedMap.get("contract_tenure_to"):null)
                                .set("status",   parsedMap.containsKey("status")?parsedMap.get("status"):null)
                                .set("pending_status",   parsedMap.containsKey("pending_status")?parsedMap.get("pending_status"):null)
                                .set("bound_type",   parsedMap.containsKey("bound_type")?parsedMap.get("bound_type"):null)
                                .set("service_type",   parsedMap.containsKey("service_type")?parsedMap.get("service_type"):null)
                                .set("branch_id",   parsedMap.containsKey("branch_id")?parsedMap.get("branch_id"):null)
                                .set("base_volume",   parsedMap.containsKey("base_volume")?parsedMap.get("base_volume"):null)
                                .set("rfq_id",   parsedMap.containsKey("rfq_id")?parsedMap.get("rfq_id"):null)
                                .set("rfq_number",   parsedMap.containsKey("rfq_number")?parsedMap.get("rfq_number"):null)
                                .set("trip_id",   parsedMap.containsKey("trip_id")?parsedMap.get("trip_id"):null)
                                .set("consigner_name",   parsedMap.containsKey("consigner_name")?parsedMap.get("consigner_name"):null)
                                .set("from",   parsedMap.containsKey("from")?parsedMap.get("from"):null)
                                .set("to",   parsedMap.containsKey("to")?parsedMap.get("to"):null)
                                .set("vehicle_specification",   parsedMap.containsKey("vehicle_specification")?parsedMap.get("vehicle_specification"):new ArrayList())
                                .set("vehicle_type",   parsedMap.containsKey("vehicle_type")?parsedMap.get("vehicle_type"):null)
                                .set("vehicle_type_id",   parsedMap.containsKey("vehicle_type_id")?parsedMap.get("vehicle_type_id"):null)
                                .set("body_type",   parsedMap.containsKey("body_type")?parsedMap.get("body_type"):null)
                                .set("stops",   parsedMap.containsKey("stops")?parsedMap.get("stops"):null)
                                .set("cargo_weight",   parsedMap.containsKey("cargo_weight")?parsedMap.get("cargo_weight"):null)
                                .set("cargo_uom",   parsedMap.containsKey("cargo_uom")?parsedMap.get("cargo_uom"):null)
                                .set("cargo_uom_name",   parsedMap.containsKey("cargo_uom_name")?parsedMap.get("cargo_uom_name"):null)
                                .set("transporters",   parsedMap.containsKey("transporters")?parsedMap.get("transporters"):new ArrayList())
                                .set("transporter_ids",   parsedMap.containsKey("transporter_ids")?parsedMap.get("transporter_ids"):new ArrayList())
                                .set("created",   parsedMap.containsKey("created")?parsedMap.get("created"):null)
                                .set("created_date",   parsedMap.containsKey("created_date")?parsedMap.get("created_date"):null)
                                .set("cargo_details",   parsedMap.containsKey("cargo_details")?parsedMap.get("cargo_details"):null)
                                .set("customer_name",   parsedMap.containsKey("customer_name")?parsedMap.get("customer_name"):null)
                                .set("distance",   parsedMap.containsKey("distance")?parsedMap.get("distance"):null)
                                .set("distance_uom",   parsedMap.containsKey("distance_uom")?parsedMap.get("distance_uom"):null)
                                .set("handling_instructions",   parsedMap.containsKey("handling_instructions")?parsedMap.get("handling_instructions"):null)
                                .set("loading_point",   parsedMap.containsKey("loading_point")?parsedMap.get("loading_point"):new ArrayList())
                                .set("lpp",   parsedMap.containsKey("lpp")?parsedMap.get("lpp"):null)
                                .set("package_type",   parsedMap.containsKey("package_type")?parsedMap.get("package_type"):null)
                                .set("payment_cycle",   parsedMap.containsKey("payment_cycle")?parsedMap.get("payment_cycle"):null)
                                .set("payment_terms",   parsedMap.containsKey("payment_terms")?parsedMap.get("payment_terms"):null)
                                .set("pricing_basis",   parsedMap.containsKey("pricing_basis")?parsedMap.get("pricing_basis"):null)
                                .set("product_category",   parsedMap.containsKey("product_category")?parsedMap.get("product_category"):null)
                                .set("rfq_trip_data_updated",   parsedMap.containsKey("rfq_trip_data_updated")?parsedMap.get("rfq_trip_data_updated"):null)
                                .set("rfq_trip_data_updated_on",   parsedMap.containsKey("rfq_trip_data_updated_on")?parsedMap.get("rfq_trip_data_updated_on"):null)
                                .set("un_loading_point",   parsedMap.containsKey("un_loading_point")?parsedMap.get("un_loading_point"):new ArrayList())
                                .set("updated_on",   parsedMap.containsKey("updated_on")?parsedMap.get("updated_on"):null)
                                .set("is_admin",   parsedMap.containsKey("is_admin")?parsedMap.get("is_admin"):null)
                                .set("company_name",   parsedMap.containsKey("company_name")?parsedMap.get("company_name"):null)
                                .set("user_id",   parsedMap.containsKey("user_id")?parsedMap.get("user_id"):null)
                                .set("branch_name",   parsedMap.containsKey("branch_name")?parsedMap.get("branch_name"):null)
                                .set("floated_by",   parsedMap.containsKey("floated_by")?parsedMap.get("floated_by"):null)
                                .set("show_tracking",   parsedMap.containsKey("show_tracking")?parsedMap.get("show_tracking"):null)
                                .set("branch_code",   parsedMap.containsKey("branch_code")?parsedMap.get("branch_code"):null)
                                .set("indent_type",   parsedMap.containsKey("indent_type")?parsedMap.get("indent_type"):null)
                                .set("dest_bound_type",   parsedMap.containsKey("dest_bound_type")?parsedMap.get("dest_bound_type"):null)
                                .set("is_milk_run",   parsedMap.containsKey("is_milk_run")?parsedMap.get("is_milk_run"):null)
                                .set("consigners",   parsedMap.containsKey("consigners")?parsedMap.get("consigners"):null)
                                .set("consignees",   parsedMap.containsKey("consignees")?parsedMap.get("consignees"):null)
                                .set("trucks_count",   parsedMap.containsKey("trucks_count")?parsedMap.get("trucks_count"):null)
                                .set("weight",   parsedMap.containsKey("weight")?parsedMap.get("weight"):null)
                                .set("quantity",   parsedMap.containsKey("quantity")?parsedMap.get("quantity"):null)
                                .set("quantity_uom",   parsedMap.containsKey("quantity_uom")?parsedMap.get("quantity_uom"):null)
                                .set("tat",   parsedMap.containsKey("tat")?parsedMap.get("tat"):null)
                                .set("tat_value",   parsedMap.containsKey("tat_value")?parsedMap.get("tat_value"):null)
                                .set("tat_uom",   parsedMap.containsKey("tat_uom")?parsedMap.get("tat_uom"):null)
                                .set("delivery_expected_date_from",   parsedMap.containsKey("delivery_expected_date_from")?parsedMap.get("delivery_expected_date_from"):null)
                                .set("delivery_expected_date_to",   parsedMap.containsKey("delivery_expected_date_to")?parsedMap.get("delivery_expected_date_to"):null)
                                .set("bonus",   parsedMap.containsKey("bonus")?parsedMap.get("bonus"):null)
                                .set("vehicle_reporting_from",   parsedMap.containsKey("vehicle_reporting_from")?parsedMap.get("vehicle_reporting_from"):null)
                                .set("vehicle_reporting_to",   parsedMap.containsKey("vehicle_reporting_to")?parsedMap.get("vehicle_reporting_to"):null)
                                .set("indent_cut_off_from",   parsedMap.containsKey("indent_cut_off_from")?parsedMap.get("indent_cut_off_from"):null)
                                .set("indent_cut_off_to",   parsedMap.containsKey("indent_cut_off_to")?parsedMap.get("indent_cut_off_to"):null)
                                .set("do_raised_date",   parsedMap.containsKey("do_raised_date")?parsedMap.get("do_raised_date"):null)
                                .set("do_raised_by",   parsedMap.containsKey("do_raised_by")?parsedMap.get("do_raised_by"):null)
                                .set("do_raised_by_name",   parsedMap.containsKey("do_raised_by_name")?parsedMap.get("do_raised_by_name"):null)
                                .set("seeker_remarks",   parsedMap.containsKey("seeker_remarks")?parsedMap.get("seeker_remarks"):null)
                                .set("bonus_amount",   parsedMap.containsKey("bonus_amount")?parsedMap.get("bonus_amount"):null)
                                .set("bonus_remarks",   parsedMap.containsKey("bonus_remarks")?parsedMap.get("bonus_remarks"):null)
                                .set("permit_number",   parsedMap.containsKey("permit_number")?parsedMap.get("permit_number"):null)
                                .set("permit_validity_date",   parsedMap.containsKey("permit_validity_date")?parsedMap.get("permit_validity_date"):null)
                                .set("permit_expiry_date",   parsedMap.containsKey("permit_expiry_date")?parsedMap.get("permit_expiry_date"):null)
                                .set("is_express_indent",   parsedMap.containsKey("is_express_indent")?parsedMap.get("is_express_indent"):null)
                                .set("flow_id",   parsedMap.containsKey("flow_id")?parsedMap.get("flow_id"):null)
                                .set("move_id",   parsedMap.containsKey("move_id")?parsedMap.get("move_id"):null)
                                .set("is_third_party_inward",   parsedMap.containsKey("is_third_party_inward")?parsedMap.get("is_third_party_inward"):null)
                                .set("indent_number",   parsedMap.containsKey("indent_number")?parsedMap.get("indent_number"):null)
                                .set("packing_type",   parsedMap.containsKey("packing_type")?parsedMap.get("packing_type"):null)
                                .set("service_mode",   parsedMap.containsKey("service_mode")?parsedMap.get("service_mode"):null)
                                .set("vehicle_required_date",   parsedMap.containsKey("vehicle_required_date")?parsedMap.get("vehicle_required_date"):null)
                                .set("vehicle_request_date",   parsedMap.containsKey("vehicle_request_date")?parsedMap.get("vehicle_request_date"):null)
                                .set("raised",   parsedMap.containsKey("raised")?parsedMap.get("raised"):new ArrayList())
                                .set("parent_indent_id",   parsedMap.containsKey("parent_indent_id")?parsedMap.get("parent_indent_id"):null)
                                .set("sub_plants",   parsedMap.containsKey("sub_plants")?parsedMap.get("sub_plants"):new ArrayList())
                                .set("truck_sequence",   parsedMap.containsKey("truck_sequence")?parsedMap.get("truck_sequence"):null)
                                .set("transporter_id",   parsedMap.containsKey("transporter_id")?parsedMap.get("transporter_id"):null)
                                .set("transporter_name",   parsedMap.containsKey("transporter_name")?parsedMap.get("transporter_name"):null)
                               // .set("trip_number",   parsedMap.containsKey("trip_number")?parsedMap.get("trip_number"):null)
                                .set("vehicle_order_id",   parsedMap.containsKey("vehicle_order_id")?parsedMap.get("vehicle_order_id"):null)
                                .set("pickup_accounts",   parsedMap.containsKey("pickup_accounts")?parsedMap.get("pickup_accounts"):new ArrayList())
                                .set("drop_accounts",   parsedMap.containsKey("drop_accounts")?parsedMap.get("drop_accounts"):new ArrayList())
                                .set("total_pickup_drop_points",   parsedMap.containsKey("total_pickup_drop_points")?parsedMap.get("total_pickup_drop_points"):null)
                                .set("status_list",   parsedMap.containsKey("status_list")?parsedMap.get("status_list"):new ArrayList())
                                .set("current_status",   parsedMap.containsKey("current_status")?parsedMap.get("current_status"):null)
                                .set("sms_count",   parsedMap.containsKey("sms_count")?parsedMap.get("sms_count"):null)
                                .set("do_accepted_date",   parsedMap.containsKey("do_accepted_date")?parsedMap.get("do_accepted_date"):null)
                                .set("previous_status",   parsedMap.containsKey("previous_status")?parsedMap.get("previous_status"):null)
                                .set("time_diff_btw_accept_or_reject_and_raised",   parsedMap.containsKey("time_diff_btw_accept_or_reject_and_raised")?parsedMap.get("time_diff_btw_accept_or_reject_and_raised"):null)
                                .set("chasis_no",   parsedMap.containsKey("chasis_no")?parsedMap.get("chasis_no"):null)
                                .set("dl_no",   parsedMap.containsKey("dl_no")?parsedMap.get("dl_no"):null)
                                .set("driver_id",   parsedMap.containsKey("driver_id")?parsedMap.get("driver_id"):null)
                                .set("driver_name",   parsedMap.containsKey("driver_name")?parsedMap.get("driver_name"):null)
                                .set("driver_no",   parsedMap.containsKey("driver_no")?parsedMap.get("driver_no"):null)
                                .set("engine_no",   parsedMap.containsKey("engine_no")?parsedMap.get("engine_no"):null)
                                .set("has_compartment",   parsedMap.containsKey("has_compartment")?parsedMap.get("has_compartment"):null)
                                .set("indent_allocation_id",   parsedMap.containsKey("indent_allocation_id")?parsedMap.get("indent_allocation_id"):null)
                                .set("is_allocated",   parsedMap.containsKey("is_allocated")?parsedMap.get("is_allocated"):null)
                                .set("lr_no",   parsedMap.containsKey("lr_no")?parsedMap.get("lr_no"):null)
                                .set("operator_details",   parsedMap.containsKey("operator_details")?parsedMap.get("operator_details"):null)
                                .set("requested_vehicle_type",   parsedMap.containsKey("requested_vehicle_type")?parsedMap.get("requested_vehicle_type"):null)
                                .set("time_diff_btw_allocation_and_accepted",   parsedMap.containsKey("time_diff_btw_allocation_and_accepted")?parsedMap.get("time_diff_btw_allocation_and_accepted"):null)
                                
                                .set("tracking_details",   parsedMap.containsKey("tracking_details")?parsedMap.get("tracking_details"):null)
                                .set("user_name",   parsedMap.containsKey("user_name")?parsedMap.get("user_name"):null)
                                .set("vehicle_alloted_date",   parsedMap.containsKey("vehicle_alloted_date")?parsedMap.get("vehicle_alloted_date"):null)
                                .set("vehicle_reg_no",   parsedMap.containsKey("vehicle_reg_no")?parsedMap.get("vehicle_reg_no"):null)
                                .set("vehicles_id",   parsedMap.containsKey("vehicles_id")?parsedMap.get("vehicles_id"):null)
                                .set("vims_trip_id",   parsedMap.containsKey("vims_trip_id")?parsedMap.get("vims_trip_id"):null)
                                .set("vims_error",   parsedMap.containsKey("vims_error")?parsedMap.get("vims_error"):null)
                                .set("vims_request_before_integration",   parsedMap.containsKey("vims_request_before_integration")?parsedMap.get("vims_request_before_integration"):null)
                                .set("vims_response_data",   parsedMap.containsKey("vims_response_data")?parsedMap.get("vims_response_data"):null)
                                .set("vims_url",   parsedMap.containsKey("vims_url")?parsedMap.get("vims_url"):null)
                                .set("is_vehicle_placed",   parsedMap.containsKey("is_vehicle_placed")?parsedMap.get("is_vehicle_placed"):null)
                                .set("time_diff_btw_placement_and_allocation",   parsedMap.containsKey("time_diff_btw_placement_and_allocation")?parsedMap.get("time_diff_btw_placement_and_allocation"):null)
                                .set("vehicle_placed_date",   parsedMap.containsKey("vehicle_placed_date")?parsedMap.get("vehicle_placed_date"):null)
                                .set("vims_consent_err_msg",   parsedMap.containsKey("vims_consent_err_msg")?parsedMap.get("vims_consent_err_msg"):null)
                                .set("vims_consent_status",   parsedMap.containsKey("vims_consent_status")?parsedMap.get("vims_consent_status"):null)
                                .set("is_depot",   parsedMap.containsKey("is_depot")?parsedMap.get("is_depot"):null)
                                .set("is_plant",   parsedMap.containsKey("is_plant")?parsedMap.get("is_plant"):null)
                                .set("is_toll_unit",   parsedMap.containsKey("is_toll_unit")?parsedMap.get("is_toll_unit"):null)
                                .set("plant_category",   parsedMap.containsKey("plant_category")?parsedMap.get("plant_category"):null)
                                .set("time_diff_btw_source_gate_in_and_vehicle_placement",   parsedMap.containsKey("time_diff_btw_source_gate_in_and_vehicle_placement")?parsedMap.get("time_diff_btw_source_gate_in_and_vehicle_placement"):null)
                                .set("token_generated_by",   parsedMap.containsKey("token_generated_by")?parsedMap.get("token_generated_by"):null)
                                .set("token_generated_by_Id",   parsedMap.containsKey("token_generated_by_Id")?parsedMap.get("token_generated_by_Id"):null)
                                .set("token_generation_time",   parsedMap.containsKey("token_generation_time")?parsedMap.get("token_generation_time"):null)
                                .set("vehicle_gate_in_time",   parsedMap.containsKey("vehicle_gate_in_time")?parsedMap.get("vehicle_gate_in_time"):null)
                                .set("unmanned_token_success",   parsedMap.containsKey("unmanned_token_success")?parsedMap.get("unmanned_token_success"):null)
                                .set("invoice_remarks",   parsedMap.containsKey("invoice_remarks")?parsedMap.get("invoice_remarks"):null)
                                .set("time_diff_btw_ext_parking_and_fetch_do",   parsedMap.containsKey("time_diff_btw_ext_parking_and_fetch_do")?parsedMap.get("time_diff_btw_ext_parking_and_fetch_do"):null)
                                .set("fetch_do_checklist_done_by",   parsedMap.containsKey("fetch_do_checklist_done_by")?parsedMap.get("fetch_do_checklist_done_by"):null)
                                .set("fetch_do_checklist_done_id",   parsedMap.containsKey("fetch_do_checklist_done_id")?parsedMap.get("fetch_do_checklist_done_id"):null)
                                .set("fetch_do_checklist_time",   parsedMap.containsKey("fetch_do_checklist_time")?parsedMap.get("fetch_do_checklist_time"):null)
                                .set("is_popup_confirm",   parsedMap.containsKey("is_popup_confirm")?parsedMap.get("is_popup_confirm"):null)
                                .set("qc_form",   parsedMap.containsKey("qc_form")?parsedMap.get("qc_form"):null)
                                .set("source_cl11",   parsedMap.containsKey("source_cl11")?parsedMap.get("source_cl11"):new ArrayList())
                                .set("doNo",   parsedMap.containsKey("doNo")?parsedMap.get("doNo"):new ArrayList())
                                .set("is_multiplant",   parsedMap.containsKey("is_multiplant")?parsedMap.get("is_multiplant"):null)
                                .set("consignee_code",   parsedMap.containsKey("consignee_code")?parsedMap.get("consignee_code"):new ArrayList())
                                .set("data_for_shipment_cost",   parsedMap.containsKey("data_for_shipment_cost")?parsedMap.get("data_for_shipment_cost"):new ArrayList())
                                .set("delivery_nos",   parsedMap.containsKey("delivery_nos")?parsedMap.get("delivery_nos"):null)
                                .set("do_weight",   parsedMap.containsKey("do_weight")?parsedMap.get("do_weight"):null)
                                .set("routeCodes",   parsedMap.containsKey("routeCodes")?parsedMap.get("routeCodes"):null)
                                .set("sap_shipment_document_nos",   parsedMap.containsKey("sap_shipment_document_nos")?parsedMap.get("sap_shipment_document_nos"):null)
                                .set("shipment_doc_generation_by",   parsedMap.containsKey("shipment_doc_generation_by")?parsedMap.get("shipment_doc_generation_by"):null)
                                .set("shipment_doc_generation_by_userId",   parsedMap.containsKey("shipment_doc_generation_by_userId")?parsedMap.get("shipment_doc_generation_by_userId"):null)
                                .set("shipment_doc_generation_time",   parsedMap.containsKey("shipment_doc_generation_time")?parsedMap.get("shipment_doc_generation_time"):null)
                                .set("sku_list",   parsedMap.containsKey("sku_list")?parsedMap.get("sku_list"):new ArrayList())
                                .set("warehouse_ids",   parsedMap.containsKey("warehouse_ids")?parsedMap.get("warehouse_ids"):new ArrayList())
                                .set("warehouse_list",   parsedMap.containsKey("warehouse_list")?parsedMap.get("warehouse_list"):null)
                                .set("weighbridge_no",   parsedMap.containsKey("weighbridge_no")?parsedMap.get("weighbridge_no"):null)
                                .set("loading_start_time",   parsedMap.containsKey("loading_start_time")?parsedMap.get("loading_start_time"):null)
                                .set("source_gate_in_time",   parsedMap.containsKey("source_gate_in_time")?parsedMap.get("source_gate_in_time"):null)
                                .set("time_diff_btw_fetch_do_and_gate_in",   parsedMap.containsKey("time_diff_btw_fetch_do_and_gate_in")?parsedMap.get("time_diff_btw_fetch_do_and_gate_in"):null)
                                .set("time_diff_btw_loading_start_and_source_gate_in",   parsedMap.containsKey("time_diff_btw_loading_start_and_source_gate_in")?parsedMap.get("time_diff_btw_loading_start_and_source_gate_in"):null)
                                .set("is_tracking_start",   parsedMap.containsKey("is_tracking_start")?parsedMap.get("is_tracking_start"):null)
                                .set("tracking_start_datetime",   parsedMap.containsKey("tracking_start_datetime")?parsedMap.get("tracking_start_datetime"):null)
                                .set("gate_in_checklist_done_by",   parsedMap.containsKey("gate_in_checklist_done_by")?parsedMap.get("gate_in_checklist_done_by"):null)
                                .set("gate_in_checklist_done_id",   parsedMap.containsKey("gate_in_checklist_done_id")?parsedMap.get("gate_in_checklist_done_id"):null)
                                .set("gate_in_checklist_time",   parsedMap.containsKey("gate_in_checklist_time")?parsedMap.get("gate_in_checklist_time"):null)
                                .set("source_cl2",   parsedMap.containsKey("source_cl2")?parsedMap.get("source_cl2"):new ArrayList())
                                .set("loading_end_time",   parsedMap.containsKey("loading_end_time")?parsedMap.get("loading_end_time"):null)
                                .set("lr_created_date",   parsedMap.containsKey("lr_created_date")?parsedMap.get("lr_created_date"):null)
                                .set("tare_weight_start",   parsedMap.containsKey("tare_weight_start")?parsedMap.get("tare_weight_start"):null)
                                .set("time_diff_btw_gate_in_and_tare_weight",   parsedMap.containsKey("time_diff_btw_gate_in_and_tare_weight")?parsedMap.get("time_diff_btw_gate_in_and_tare_weight"):null)
                                .set("time_diff_btw_loading_end_and_loading_start",   parsedMap.containsKey("time_diff_btw_loading_end_and_loading_start")?parsedMap.get("time_diff_btw_loading_end_and_loading_start"):null)
                                .set("TAREAT",   parsedMap.containsKey("TAREAT")?parsedMap.get("TAREAT"):null)
                                .set("TAREWT",   parsedMap.containsKey("TAREWT")?parsedMap.get("TAREWT"):null)
                                .set("Weight_Operator",   parsedMap.containsKey("Weight_Operator")?parsedMap.get("Weight_Operator"):null)
                                .set("time_diff_btw_tare_weight_and_preload_parking",   parsedMap.containsKey("time_diff_btw_tare_weight_and_preload_parking")?parsedMap.get("time_diff_btw_tare_weight_and_preload_parking"):null)
                                .set("bay_gang_assign_by",   parsedMap.containsKey("bay_gang_assign_by")?parsedMap.get("bay_gang_assign_by"):null)
                                .set("bay_gang_assign_id",   parsedMap.containsKey("bay_gang_assign_id")?parsedMap.get("bay_gang_assign_id"):null)
                                .set("bay_gang_assign_time",   parsedMap.containsKey("bay_gang_assign_time")?parsedMap.get("bay_gang_assign_time"):null)
                                .set("bay_id",   parsedMap.containsKey("bay_id")?parsedMap.get("bay_id"):null)
                                .set("loading_bay_name",   parsedMap.containsKey("loading_bay_name")?parsedMap.get("loading_bay_name"):null)
                                .set("loading_vendor_name",   parsedMap.containsKey("loading_vendor_name")?parsedMap.get("loading_vendor_name"):null)
                                .set("pre_load_parking_remarks",   parsedMap.containsKey("pre_load_parking_remarks")?parsedMap.get("pre_load_parking_remarks"):null)
                                .set("pre_warehouse_id",   parsedMap.containsKey("pre_warehouse_id")?parsedMap.get("pre_warehouse_id"):null)
                                .set("vendor_id",   parsedMap.containsKey("vendor_id")?parsedMap.get("vendor_id"):null)
                                .set("time_diff_btw_preload_parking_and_loading",   parsedMap.containsKey("time_diff_btw_preload_parking_and_loading")?parsedMap.get("time_diff_btw_preload_parking_and_loading"):null)
                                .set("loading_checklist_lp_done_by",   parsedMap.containsKey("loading_checklist_lp_done_by")?parsedMap.get("loading_checklist_lp_done_by"):null)
                                .set("loading_checklist_lp_done_id",   parsedMap.containsKey("loading_checklist_lp_done_id")?parsedMap.get("loading_checklist_lp_done_id"):null)
                                .set("loading_checklist_time",   parsedMap.containsKey("loading_checklist_time")?parsedMap.get("loading_checklist_time"):null)
                                .set("source_cl12",   parsedMap.containsKey("source_cl12")?parsedMap.get("source_cl12"):new ArrayList())
                                .set("is_loading_saved",   parsedMap.containsKey("is_loading_saved")?parsedMap.get("is_loading_saved"):null)
                                .set("is_loading_saved_time",   parsedMap.containsKey("is_loading_saved_time")?parsedMap.get("is_loading_saved_time"):null)
                                .set("time_diff_btw_loading_and_pgi",   parsedMap.containsKey("time_diff_btw_loading_and_pgi")?parsedMap.get("time_diff_btw_loading_and_pgi"):null)
                                .set("pgi_time",   parsedMap.containsKey("pgi_time")?parsedMap.get("pgi_time"):null)
                                .set("pgi_complete",   parsedMap.containsKey("pgi_complete")?parsedMap.get("pgi_complete"):null)
                                .set("pgi_done_by",   parsedMap.containsKey("pgi_done_by")?parsedMap.get("pgi_done_by"):null)
                                .set("pgi_remarks",   parsedMap.containsKey("pgi_remarks")?parsedMap.get("pgi_remarks"):null)
                                .set("pgi_response",   parsedMap.containsKey("pgi_response")?parsedMap.get("pgi_response"):new ArrayList())
                                .set("gross_weight_start",   parsedMap.containsKey("gross_weight_start")?parsedMap.get("gross_weight_start"):null)
                                .set("time_diff_btw_pgi_and_grossweight",   parsedMap.containsKey("time_diff_btw_pgi_and_grossweight")?parsedMap.get("time_diff_btw_pgi_and_grossweight"):null)
                                .set("GROSSAT",   parsedMap.containsKey("GROSSAT")?parsedMap.get("GROSSAT"):null)
                                .set("GROSSWT",   parsedMap.containsKey("GROSSWT")?parsedMap.get("GROSSWT"):null)
                                .set("time_diff_grossWeight_and_post_load_parking",   parsedMap.containsKey("time_diff_grossWeight_and_post_load_parking")?parsedMap.get("time_diff_grossWeight_and_post_load_parking"):null)
                                .set("time_diff_postload_parking_and_docdocket",   parsedMap.containsKey("time_diff_postload_parking_and_docdocket")?parsedMap.get("time_diff_postload_parking_and_docdocket"):null)
                                .set("Shipment_cost_data_response",   parsedMap.containsKey("Shipment_cost_data_response")?parsedMap.get("Shipment_cost_data_response"):null)
                                .set("invoice_data",   parsedMap.containsKey("invoice_data")?parsedMap.get("invoice_data"):new ArrayList())
                                .set("invoice_response",   parsedMap.containsKey("invoice_response")?parsedMap.get("invoice_response"):null)
                                .set("is_fetch_invoice",   parsedMap.containsKey("is_fetch_invoice")?parsedMap.get("is_fetch_invoice"):null)
                                .set("shipment_cost_doc_generation_time",   parsedMap.containsKey("shipment_cost_doc_generation_time")?parsedMap.get("shipment_cost_doc_generation_time"):null)
                                .set("shipment_cost_response",   parsedMap.containsKey("shipment_cost_response")?parsedMap.get("shipment_cost_response"):null)
                                .set("time_diff_docdocket_and_gateout",   parsedMap.containsKey("time_diff_docdocket_and_gateout")?parsedMap.get("time_diff_docdocket_and_gateout"):null)
                                .set("source_cl10",   parsedMap.containsKey("source_cl10")?parsedMap.get("source_cl10"):new ArrayList())
                                .set("source_gate_out_checklist__time",   parsedMap.containsKey("source_gate_out_checklist__time")?parsedMap.get("source_gate_out_checklist__time"):null)
                                .set("source_gate_out_done_by",   parsedMap.containsKey("source_gate_out_done_by")?parsedMap.get("source_gate_out_done_by"):null)
                                .set("source_gate_out_done_id",   parsedMap.containsKey("source_gate_out_done_id")?parsedMap.get("source_gate_out_done_id"):null)
                                .set("destination_move_id",   parsedMap.containsKey("destination_move_id")?parsedMap.get("destination_move_id"):null)
                                .set("gate_out_done_by",   parsedMap.containsKey("gate_out_done_by")?parsedMap.get("gate_out_done_by"):null)
                                .set("is_vehicle_at_source",   parsedMap.containsKey("is_vehicle_at_source")?parsedMap.get("is_vehicle_at_source"):null)
                                .set("source_gate_out_time",   parsedMap.containsKey("source_gate_out_time")?parsedMap.get("source_gate_out_time"):null)
                                .set("tat_time",   parsedMap.containsKey("tat_time")?parsedMap.get("tat_time"):null)
                                .set("time_diff_btw_source_gate_out_and_loading_end",   parsedMap.containsKey("time_diff_btw_source_gate_out_and_loading_end")?parsedMap.get("time_diff_btw_source_gate_out_and_loading_end"):null)
                                .set("dest_arrived_datetime",   parsedMap.containsKey("dest_arrived_datetime")?parsedMap.get("dest_arrived_datetime"):null)
                                .set("is_destination_arrived",   parsedMap.containsKey("is_destination_arrived")?parsedMap.get("is_destination_arrived"):null)

                                .set("link_po_token",   parsedMap.containsKey("link_po_token")?parsedMap.get("link_po_token"):null)
                                .set("fetch_po_done_time",   parsedMap.containsKey("fetch_po_done_time")?parsedMap.get("fetch_po_done_time"):null)
                                .set("is_fetch_po_done",   parsedMap.containsKey("is_fetch_po_done")?parsedMap.get("is_fetch_po_done"):null)
                                .set("po_no_list",   parsedMap.containsKey("po_no_list")?parsedMap.get("po_no_list"):new ArrayList())
                                .set("purchase_order_no",   parsedMap.containsKey("purchase_order_no")?parsedMap.get("purchase_order_no"):null)
                                .set("shipping_type",   parsedMap.containsKey("shipping_type")?parsedMap.get("shipping_type"):null)
                                .set("destination_gate_in_time",   parsedMap.containsKey("destination_gate_in_time")?parsedMap.get("destination_gate_in_time"):null)
                                .set("time_diff_btw_dest_extpark_gate_in",   parsedMap.containsKey("time_diff_btw_dest_extpark_gate_in")?parsedMap.get("time_diff_btw_dest_extpark_gate_in"):null)
                                .set("destination_cl2",   parsedMap.containsKey("destination_cl2")?parsedMap.get("destination_cl2"):new ArrayList())
                                .set("destination_cl2done_Id",   parsedMap.containsKey("destination_cl2done_Id")?parsedMap.get("destination_cl2done_Id"):null)
                                .set("destination_cl2done_by",   parsedMap.containsKey("destination_cl2done_by")?parsedMap.get("destination_cl2done_by"):null)
                                .set("destination_cl2time",   parsedMap.containsKey("destination_cl2time")?parsedMap.get("destination_cl2time"):null)
                                .set("time_diff_btw_dest_gatein_and_preunload",   parsedMap.containsKey("time_diff_btw_dest_gatein_and_preunload")?parsedMap.get("time_diff_btw_dest_gatein_and_preunload"):null)
                                .set("destination_cl19",   parsedMap.containsKey("destination_cl19")?parsedMap.get("destination_cl19"):new ArrayList())
                                .set("destination_cl19done_Id",   parsedMap.containsKey("destination_cl19done_Id")?parsedMap.get("destination_cl19done_Id"):null)
                                .set("destination_cl19done_by",   parsedMap.containsKey("destination_cl19done_by")?parsedMap.get("destination_cl19done_by"):null)
                                .set("destination_cl19time",   parsedMap.containsKey("destination_cl19time")?parsedMap.get("destination_cl19time"):null)
                                .set("grn_nos",   parsedMap.containsKey("grn_nos")?parsedMap.get("grn_nos"):new ArrayList())
                                .set("grn_req_no_depot",   parsedMap.containsKey("grn_req_no_depot")?parsedMap.get("grn_req_no_depot"):null)
                                .set("is_unloading_saved",   parsedMap.containsKey("is_unloading_saved")?parsedMap.get("is_unloading_saved"):null)
                                .set("unloading_saved_by",   parsedMap.containsKey("unloading_saved_by")?parsedMap.get("unloading_saved_by"):null)
                                .set("unloading_saved_time",   parsedMap.containsKey("unloading_saved_time")?parsedMap.get("unloading_saved_time"):null)
                                .set("cbb_status",   parsedMap.containsKey("cbb_status")?parsedMap.get("cbb_status"):null)
                                .set("depot_grn_request_time",   parsedMap.containsKey("depot_grn_request_time")?parsedMap.get("depot_grn_request_time"):null)
                                .set("is_ccb",   parsedMap.containsKey("is_ccb")?parsedMap.get("is_ccb"):null)
                                .set("is_manual_grn",   parsedMap.containsKey("is_manual_grn")?parsedMap.get("is_manual_grn"):null)
                                .set("is_grn_done",   parsedMap.containsKey("is_grn_done")?parsedMap.get("is_grn_done"):null)
                                .set("depot_grn_time",   parsedMap.containsKey("depot_grn_time")?parsedMap.get("depot_grn_time"):null)
                                .set("action",   parsedMap.containsKey("action")?parsedMap.get("action"):null)
                                .set("closed_date_by",   parsedMap.containsKey("closed_date_by")?parsedMap.get("closed_date_by"):null)
                                .set("closed_date_by_vims",   parsedMap.containsKey("closed_date_by_vims")?parsedMap.get("closed_date_by_vims"):null)
                                .set("vims_closure_remarks",   parsedMap.containsKey("vims_closure_remarks")?parsedMap.get("vims_closure_remarks"):null)
                                .set("time_diff_btw_dest_unload_grn_and_gateout",   parsedMap.containsKey("time_diff_btw_dest_unload_grn_and_gateout")?parsedMap.get("time_diff_btw_dest_unload_grn_and_gateout"):null)
                                .set("destination_cl10",   parsedMap.containsKey("destination_cl10")?parsedMap.get("destination_cl10"):new ArrayList())
                                .set("destination_cl10done_Id",   parsedMap.containsKey("destination_cl10done_Id")?parsedMap.get("destination_cl10done_Id"):null)
                                .set("destination_cl10done_by",   parsedMap.containsKey("destination_cl10done_by")?parsedMap.get("destination_cl10done_by"):null)
                                .set("destination_cl10time",   parsedMap.containsKey("destination_cl10time")?parsedMap.get("destination_cl10time"):null)
                                .set("destination_gate_out_done_by",   parsedMap.containsKey("destination_gate_out_done_by")?parsedMap.get("destination_gate_out_done_by"):null)
                                .set("destination_gate_out_time",   parsedMap.containsKey("destination_gate_out_time")?parsedMap.get("destination_gate_out_time"):null)
                                .set("time_diff_btw_dest_gate_out_and_dest_unloading_end",   parsedMap.containsKey("time_diff_btw_dest_gate_out_and_dest_unloading_end")?parsedMap.get("time_diff_btw_dest_gate_out_and_dest_unloading_end"):null)
                                .set("moved_to_accounts",   parsedMap.containsKey("moved_to_accounts")?parsedMap.get("moved_to_accounts"):null);
                        

                                
                             /*  Iterator<Entry<String, Object>> iterator = parsedMap.entrySet().iterator();
                                while (iterator.hasNext()) {
                                    Entry<String, Object> entry = iterator.next();
                                    System.out.println("entry...."+entry);
                                    row.set(entry.getKey(), entry.getValue());
                                }*/

                                return row;

                            }
                        }
                )).apply(
                BigQueryIO.writeTableRows()
                        .to(table1)
                        .withCreateDisposition(BigQueryIO.Write.CreateDisposition.CREATE_NEVER)
                        .withWriteDisposition(BigQueryIO.Write.WriteDisposition.WRITE_APPEND));
                       //.withCreateDisposition(BigQueryIO.Write.CreateDisposition.CREATE_IF_NEEDED).withSchema(schema)
                      // .withWriteDisposition(BigQueryIO.Write.WriteDisposition.WRITE_TRUNCATE));
                        

        pipeline.run().waitUntilFinish();
    }

    public interface PubSubToGCSOptions extends PipelineOptions, StreamingOptions, GcpOptions {
        @Description("The Cloud Pub/Sub topic to read from.")
        @Required
        String getInputTopic();

        void setInputTopic(String value);

        @Description("Output file's window size in number of minutes.")
        @Default.Integer(1)
        Integer getWindowSize();

        void setWindowSize(Integer value);

        @Description("Path of the output file including its filename prefix.")
        @Required
        String getOutput();

        void setOutput(String value);
    }
}


