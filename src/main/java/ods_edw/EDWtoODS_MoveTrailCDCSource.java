package ods_edw;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import org.bson.Document;

import com.google.api.core.ApiFuture;
import com.google.cloud.pubsub.v1.Publisher;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonElement;
import com.google.protobuf.ByteString;
import com.google.pubsub.v1.ProjectTopicName;
import com.google.pubsub.v1.PubsubMessage;
import com.mongodb.Block;
import com.mongodb.MongoClient;
import com.mongodb.MongoClientURI;
import com.mongodb.client.MongoCollection;
import com.mongodb.client.MongoDatabase;
import com.mongodb.client.model.changestream.ChangeStreamDocument;
import com.mongodb.client.model.changestream.FullDocument;

public class EDWtoODS_MoveTrailCDCSource {
	  public static void main(String... args) throws Exception {
		    String PROJECT_ID = args[0];
	        String topicId = args[1];
	        String mongoUri = args[2];
	        MongoClient mongoClient = new MongoClient(new MongoClientURI(mongoUri));
	        MongoDatabase database = mongoClient.getDatabase("teg_2_0_indent_prod");
	        MongoCollection<Document> collection = database.getCollection("indent_move_trail");
	        ProjectTopicName topicName = ProjectTopicName.of(PROJECT_ID, topicId);
	        List<ApiFuture<String>> futures = new ArrayList<>();
	        final Publisher publisher = Publisher.newBuilder(topicName).build();
	        Block<ChangeStreamDocument<Document>> printBlock = new Block<ChangeStreamDocument<Document>>() {
	            @Override
	            public void apply(final ChangeStreamDocument<Document> changeStreamDocument) {
	                try {
	                	if(changeStreamDocument.getFullDocument() !=null) {
	                		
	                		Document doc  = changeStreamDocument.getFullDocument();
	                		//   System.out.println("document...."+doc);
	                		//if(doc.getString("company_id").equals("872")) {
	                			 Gson gson = new GsonBuilder().create();
	                			 JsonElement node =  gson.toJsonTree(doc);
	                    		   //HashMap<String, Object> parsedMap = gson.fromJson(node.toString(), HashMap.class);
	                    		   Iterator<String> iterator = doc.keySet().iterator(); 
	                    		   while(iterator.hasNext()){ 
	                    		   String certification = iterator.next();
	                    		   char ch = certification.charAt(0);
	                    		   if(ch >= '0' && ch <= '9'){ 
	                    			   iterator.remove();
	                    		   } 
	                    		   }
	                    
	                    		 JsonElement nodeval =  gson.toJsonTree(doc);
	                    		 System.out.println("......"+nodeval);
	                             ByteString data = ByteString.copyFromUtf8(nodeval.toString());
	                             PubsubMessage pubsubMessage = PubsubMessage.newBuilder()
	                                     .setData(data)
	                                     .build();
	                             // Schedule a message to be published. Messages are automatically batched.
	                             ApiFuture<String> future = publisher.publish(pubsubMessage);
	                             futures.add(future);
	                		//}
	                	}
	                } catch (Exception e) {
	                    e.printStackTrace();
	                } finally {

	                }
	            }
	        };
	        collection.watch().fullDocument(FullDocument.UPDATE_LOOKUP).forEach(printBlock);
	  }

}
