package ods_edw;

import java.io.IOException;
import java.util.HashMap;

import org.apache.beam.sdk.Pipeline;
import org.apache.beam.sdk.extensions.gcp.options.GcpOptions;
import org.apache.beam.sdk.io.gcp.bigquery.BigQueryIO;
import org.apache.beam.sdk.io.gcp.pubsub.PubsubIO;
import org.apache.beam.sdk.options.Default;
import org.apache.beam.sdk.options.Description;
import org.apache.beam.sdk.options.PipelineOptions;
import org.apache.beam.sdk.options.PipelineOptionsFactory;
import org.apache.beam.sdk.options.StreamingOptions;
import org.apache.beam.sdk.options.Validation.Required;
import org.apache.beam.sdk.transforms.MapElements;
import org.apache.beam.sdk.transforms.SimpleFunction;

import com.google.api.services.bigquery.model.TableReference;
import com.google.api.services.bigquery.model.TableRow;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import ods_edw.ODStoEDW_MoviesCDCSink.PubSubToGCSOptions;

public class EDWtoODS_GrossCDCSink {

	 public static void main(String[] args) throws IOException {
		
		  
		  updateGrossWeight(args);
		 
	 }
	 private static void updateGrossWeight(String[] args) {
		// TODO Auto-generated method stub
		  PubSubToGCSOptions options = PipelineOptionsFactory
	                .fromArgs(args)
	                .withValidation()
	                .as(PubSubToGCSOptions.class);
		  TableReference tableGross = new TableReference();
		  tableGross.setProjectId(options.getProject());
		  tableGross.setDatasetId("dStream");
		  tableGross.setTableId("gross_weight");
		  
		   Pipeline pipeline1 = Pipeline.create(options);
		   
		   pipeline1
           .apply("Read PubSub Messages", PubsubIO.readStrings().fromTopic(options.getInputTopic()))
           .apply("Read and transform Movies data", MapElements.via(
                   new SimpleFunction<String, TableRow>() {
                       @Override
                       public TableRow apply(String document) {
                    	   Gson gson = new GsonBuilder().create();
                           //System.out.println("document...."+document);
                           HashMap<String, Object> parsedMap = gson.fromJson(document, HashMap.class);
                           //schema.putAll(parsedMap);
                           TableRow row = new TableRow();
                        		   row.putAll(parsedMap);
                           return row;
                       }
                   }
           )).apply(
           BigQueryIO.writeTableRows()
                   .to(tableGross)
                   .withCreateDisposition(BigQueryIO.Write.CreateDisposition.CREATE_NEVER)
                   .withWriteDisposition(BigQueryIO.Write.WriteDisposition.WRITE_APPEND));
                  //.withCreateDisposition(BigQueryIO.Write.CreateDisposition.CREATE_IF_NEEDED).withSchema(schema)
                 // .withWriteDisposition(BigQueryIO.Write.WriteDisposition.WRITE_TRUNCATE));
                   

   pipeline1.run().waitUntilFinish();
		
	}
	public interface PubSubToGCSOptions extends PipelineOptions, StreamingOptions, GcpOptions {
	        @Description("The Cloud Pub/Sub topic to read from.")
	        @Required
	        String getInputTopic();

	        void setInputTopic(String value);

	        @Description("Output file's window size in number of minutes.")
	        @Default.Integer(1)
	        Integer getWindowSize();

	        void setWindowSize(Integer value);

	        @Description("Path of the output file including its filename prefix.")
	        @Required
	        String getOutput();

	        void setOutput(String value);
	    }
}
